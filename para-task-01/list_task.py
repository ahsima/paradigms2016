# Remove equal adjacent elements
#
# Example input: [1, 2, 2, 3]
# Example output: [1, 2, 3]
# This implementation returns new list instead of mutating an old one
def remove_adjacent(lst):
    lst2 = []
    first = True
    for i in lst:
        if first:
            first = False
            lst2.append(i)
        elif i != prev:
            lst2.append(i)
        prev = i
    return lst2
# if we wanted to mutate the old list we can replace "return lst2"
# with lst = lst2

# Merge two sorted lists in one sorted list in linear time
#
# Example input: [2, 4, 6], [1, 3, 5]
# Example output: [1, 2, 3, 4, 5, 6]
def linear_merge(lst1, lst2):
    lstm = []
    i1 = 0
    i2 = 0
    while len(lst1) > i1 and len(lst2) > i2:
        if lst1[i1] >= lst2[i2]:
            lstm.append(lst2[i2])
            i2 += 1
        else:
            lstm.append(lst1[i1])
            i1  += 1
    lstm += lst1[i1:]
    lstm += lst2[i2:]
    return lstm

