# Шаблон для домашнѣго задания
# Рѣализуйте мѣтоды с raise NotImplementedError


class Scope:

    """Scope - представляет доступ к значениям по именам
    (к функциям и именованным константам).
    Scope может иметь родителя, и если поиск по имени
    в текущем Scope не успешен, то если у Scope есть родитель,
    то поиск делегируется родителю.
    Scope должен поддерживать dict-like интерфейс доступа
    (см. на специальные функции __getitem__ и __setitem__)
    """

    def __init__(self, parent=None):
        self.dict = {}
        self.parent = parent

    def __setitem__(self, key, item):
        self.dict[key] = item

    def __getitem__(self, key):
        if key in self.dict:
            return self.dict[key]
        elif self.parent is None:
            raise KeyError('Item "' + key +
                           '" was not found in the current scope')
        else:
            return self.parent[key]


class Number:

    """Number - представляет число в программе.
    Все числа в нашем языке целые."""

    def __init__(self, value):
        self.value = value

    def evaluate(self, scope):
        return self


class NullExpressionEvalError(Exception):

    """
    Ошибка при попытке вычислить NullExpression
    """

    pass


class NullExpression:

    """
    Выражение, которое не может быть вычислено и вызывает
    ошибку при попытке его вычислить
    """

    def __init__(self, reason="NullExpression cannot be evaluated"):
        self.reason = reason

    def evaluate(self, scope):
        raise NullExpressionEvalError(str(self.reason))


class Function:

    """Function - представляет функцию в программе.
    Функция - второй тип поддерживаемый языком.
    Функции можно передавать в другие функции,
    и возвращать из функций.
    Функция состоит из тела и списка имен аргументов.
    Тело функции это список выражений,
    т. е.  у каждого из них есть метод evaluate.
    Во время вычисления функции (метод evaluate),
    все объекты тела функции вычисляются последовательно,
    и результат вычисления последнего из них
    является результатом вычисления функции.
    Список имен аргументов - список имен
    формальных параметров функции."""

    nullexpr = NullExpression("Expression was returned from \
a function that did nothing")

    def __init__(self, args, body):
        self.args = args
        self.body = body

    def evaluate(self, scope):
        if not self.body:
            return self.nullexpr
        for expr in self.body[:-1]:
            expr.evaluate(scope)
        return self.body[-1].evaluate(scope)


class FunctionDefinition:

    """FunctionDefinition - представляет определение функции,
    т. е. связывает некоторое имя с объектом Function.
    Результатом вычисления FunctionDefinition является
    обновление текущего Scope - в него
    добавляется новое значение типа Function."""

    def __init__(self, name, function):
        self.name = name
        self.function = function

    def evaluate(self, scope):
        scope[self.name] = self.function
        return self.function


class Conditional:

    """
    Conditional - представляет ветвление в программе, т. е. if.
    """

    nullexpr = NullExpression("Expression was returned from a \
conditional that did nothing")

    def __init__(self, condition, if_true, if_false=None):
        self.condition = condition
        self.if_true = if_true
        self.if_false = if_false

    def evaluate(self, scope):
        cond = self.condition.evaluate(scope).value
        if cond:
            body = self.if_true
        else:
            body = self.if_false
        if body:
            for expr in body[:-1]:
                expr.evaluate(scope)
            return body[-1].evaluate(scope)
        else:
            return self.nullexpr


class Print:

    """Print - печатает значение выражения на отдельной строке."""

    def __init__(self, expr):
        self.expr = expr

    def evaluate(self, scope):
        number = self.expr.evaluate(scope)
        print(number.value)
        return number


class Read:

    """Read - читает число из стандартного потока ввода
     и обновляет текущий Scope.
     Каждое входное число располагается на отдельной строке
     (никаких пустых строк и лишних символов не будет).
     """

    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        number = Number(int(input()))
        scope[self.name] = number
        return number


class FunctionCall:

    """
    FunctionCall - представляет вызов функции в программе.
    В результате вызова функции должен создаваться новый объект Scope,
    являющий дочерним для текущего Scope
    (т. е. текущий Scope должен стать для него родителем).
    Новый Scope станет текущим Scope-ом при вычислении тела функции.
    """

    def __init__(self, fun_expr, args):
        self.fun_expr = fun_expr
        self.args = args

    def evaluate(self, scope):
        function = self.fun_expr.evaluate(scope)
        call_scope = Scope(scope)
        for name, arg in zip(function.args, self.args):
            call_scope[name] = arg.evaluate(scope)
        return function.evaluate(call_scope)


class Reference:

    """Reference - получение объекта
    (функции или переменной) по его имени."""

    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        return scope[self.name]


class BinaryOperation:

    """BinaryOperation - представляет бинарную операцию над двумя выражениями.
    Результатом вычисления бинарной операции является объект Number.
    Поддерживаемые операции:
    “+”, “-”, “*”, “/”, “%”, “==”, “!=”,
    “<”, “>”, “<=”, “>=”, “&&”, “||”."""

    command = {
        '+': lambda l, r: l + r,
        '-': lambda l, r: l - r,
        '*': lambda l, r: l * r,
        '/': lambda l, r: l // r,
        '%': lambda l, r: l % r,
        '==': lambda l, r: l == r,
        '!=': lambda l, r: l != r,
        '<': lambda l, r: l < r,
        '>': lambda l, r: l > r,
        '<=': lambda l, r: l <= r,
        '>=': lambda l, r: l >= r,
        '&&': lambda l, r: l and r,
        '||': lambda l, r: l or r
    }

    def __init__(self, lhs, op, rhs):
        self.lhs = lhs
        self.op = op
        self.rhs = rhs

    def evaluate(self, scope):
        lhs = self.lhs.evaluate(scope).value
        rhs = self.rhs.evaluate(scope).value
        return Number(int(self.command[self.op](lhs, rhs)))


class UnaryOperation:

    """UnaryOperation - представляет унарную операцию над выражением.
    Результатом вычисления унарной операции является объект Number.
    Поддерживаемые операции: “-”, “!”."""

    command = {
        '!': lambda x: not x,
        '-': lambda x: -x
    }

    def __init__(self, op, expr):
        self.op = op
        self.expr = expr

    def evaluate(self, scope):
        return Number(int(self.command[self.op]
                          (self.expr.evaluate(scope).value)))


def example():
    parent = Scope()
    parent["foo"] = Function(('hello', 'world'),
                             [Print(BinaryOperation(Reference('hello'),
                                                    '+',
                                                    Reference('world')))])
    parent["bar"] = Number(10)
    scope = Scope(parent)
    assert 10 == scope["bar"].value
    scope["bar"] = Number(20)
    assert scope["bar"].value == 20
    print('It should print 2: ', end=' ')
    FunctionCall(FunctionDefinition('foo', parent['foo']),
                 [Number(5), UnaryOperation('-', Number(3))]).evaluate(scope)


def my_tests():
    parent = Scope()
    scope = Scope(parent)
#   Test error handling of condition
    try:
        Conditional(Number(0), [], []).evaluate(scope).evaluate(scope)
    except NullExpressionEvalError:
        print("Condition didn't work(as expected)")
#   Simple condition test
    FunctionDefinition('choose', Function(('cond', 'if_true', 'if_false'),
                                          [Conditional(Reference('cond'),
                                                       [Reference('if_true')],
                                                       [Reference('if_false')])
                                           ])).evaluate(parent)
    print('It should print 0: ', end=' ')
    Print(FunctionCall(FunctionDefinition(
        'choose', Reference('choose').evaluate(parent)),
        [Number(0), Number(1), Number(0)]
    )).evaluate(scope)
#   Better condition test and Read test
    print('It should print 0 if you type 0 and 1 otherwise')
    Print(FunctionCall(FunctionDefinition(
        'choose', Reference('choose').evaluate(parent)),
        [Read("input"), Number(1), Number(0)]
    )).evaluate(scope)
#   Test of read and binary operation
    print("Enter two numbers and a binary operation.\
It should print result of that operation")
    Read("num1").evaluate(scope)
    Read("num2").evaluate(scope)
    op = input()
    Print(BinaryOperation(Reference("num1"), op,
                          Reference("num2"))).evaluate(scope)


if __name__ == '__main__':
    example()
    my_tests()
