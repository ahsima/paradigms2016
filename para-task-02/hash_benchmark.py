import timeit
import sys
from find_duplicates import File
import find_duplicates as fd


def hash_benchmark(input_file):
    for i in range(20):
        fd.BLOCKSIZE = 512 * 2 ** i
        start = timeit.default_timer()
        File(input_file)
        end = timeit.default_timer()
        print(fd.BLOCKSIZE, end-start)


def main():
    if len(sys.argv) < 2:
        print("No input argument")
        sys.exit(1)
    hash_benchmark(sys.argv[1])


if __name__ == '__main__':
    main()
