import hashlib
import sys
import os

BLOCK_EXP = 7
BLOCKSIZE = 512 * 2 ** BLOCK_EXP

"""
Про размер блока.
C помощью функции hash_benchmark я установил оптимальный размер блока
для чтения, но он может отличаться в зависимости от характеристик компьютера
и размеров входного фаила. Эту функцию я вынес в отдельный фаил
"""


class File:
    def __init__(self, filepath):
        self.path = filepath
        self.dir = os.path.dirname(filepath)
        self.name = os.path.basename(filepath)
        self.update_hash()

    def update_hash(self):
        hash = hashlib.sha1()
        with open(self.path, 'rb') as f:
            while True:
                s = f.read(BLOCKSIZE)
                if s == b"":
                    break
                hash.update(s)
        self.hashstring = hash.hexdigest()

    def get_hash(self):
        return self.hashstring

    def get_name(self):
        return self.name

    def get_path(self):
        return self.path

    def __str__(self):
        return self.path

    def __eq__(self, value):
        return self.hashstring == value.get_hash() or \
                self.name == value.get_name()


def print_all_files(files):
    print("\n".join([":".join([str(i) for i in j])
                     for j in list(files.values()) if len(j) > 1]))


def read_files(start_path):
    files = []
    for path, _, filenames in os.walk(start_path):
        for filename in filenames:
            if filename[0] not in ['~', '.'] and\
                    not os.path.islink(os.path.join(path, filename)):
                files.append(File(os.path.join(os.path.abspath(path),
                                               filename)))
    return files


def find_duplicates(files):
    duplicates = {}
    for file in files:
        if file.get_hash() in duplicates:
            duplicates[file.get_hash()].append(file)
        else:
            duplicates[file.get_hash()] = [file]
    return duplicates


def main():
    if len(sys.argv) != 2:
        print("You need to specify input folder")
        sys.exit(1)
    files = read_files(sys.argv[1])
    duplicates = find_duplicates(files)
    print_all_files(duplicates)


if __name__ == '__main__':
    main()
