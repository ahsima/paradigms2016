import numpy as np

STRASSEN_MIN_SIZE = 1


def input_matrix(n):
    return np.array([(np.array([float(j) for j in input().split()]))
                     for i in range(n)])


def upsize_matrix(matrix):
    size = len(matrix)
    target = 1 << (size-1).bit_length()
    matrix = np.vstack((matrix, np.zeros([target-size, size],
                                         dtype=matrix.dtype)))
    matrix = np.hstack((matrix, np.zeros([target, target-size],
                                         dtype=matrix.dtype)))
    return matrix


def strassen(a, b):
    n = len(a)
    type = a.dtype
    if n <= STRASSEN_MIN_SIZE:
        return np.dot(a, b)
    n2 = n // 2
    a = [[a[:n2, :n2], a[:n2, n2:]], [a[n2:, :n2], a[n2:, n2:]]]
    b = [[b[:n2, :n2], b[:n2, n2:]], [b[n2:, :n2], b[n2:, n2:]]]
    res = np.empty((n, n), dtype=type)
    m = strassen((a[0][0] + a[1][1]), b[0][0] + b[1][1])
    res[:n2, :n2] = m
    res[n2:, n2:] = m
    m = strassen((a[1][0] + a[1][1]), b[0][0])
    res[n2:, :n2] = m
    res[n2:, n2:] -= m
    m = strassen((a[0][0]), b[0][1] - b[1][1])
    res[:n2, n2:] = np.array(m)
    res[n2:, n2:] += m
    m = strassen((a[1][1]), -b[0][0] + b[1][0])
    res[n2:, :n2] += m
    res[:n2, :n2] += m
    m = strassen((a[0][0] + a[0][1]), b[1][1])
    res[:n2, n2:] += m
    res[:n2, :n2] -= m
    m = strassen((-a[0][0] + a[1][0]), b[0][0] + b[0][1])
    res[n2:, n2:] += m
    m = strassen((a[0][1] - a[1][1]), b[1][0] + b[1][1])
    res[:n2, :n2] += m
    return res


def print_matrix(matrix):
    print("\n".join([" ".join([str(i) for i in j]) for j in matrix]))


def main():
    n = int(input())
    mat1 = input_matrix(n)
    mat1 = upsize_matrix(mat1)
    mat2 = input_matrix(n)
    mat2 = upsize_matrix(mat2)
    mat = strassen(mat1, mat2)[:n, :n]
    print_matrix(mat)


if __name__ == '__main__':
    main()
