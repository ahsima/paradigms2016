-- 1. head' возвращает первый элемент непустого списка
head' :: [a] -> a
head' (x:xs) = x

-- 2. tail' возвращает список без первого элемента, для пустого - пустой
tail' :: [a] -> [a]
tail' [] = []
tail' (x:xs) = xs

-- 3. take' возвращает первые n >= 0 элементов исходного списка (0,5)
take' :: Int -> [a] -> [a]
take' 0 _ = []
take' _ [] = []
take' n (x:xs) = x : take (n - 1) xs

-- 4. drop' возвращает список без первых n >= 0 элементов; если n больше длины -- списка, то пустой список.
drop' :: Int -> [a] -> [a]
drop' 0 x = x
drop' _ [] = []
drop' n (x:xs) = drop' (n - 1) xs

-- 5. filter' возвращает список из элементов, для которых f возвращает True
filter' :: (a -> Bool) -> [a] -> [a]
filter' f x = [y | y<-x, f y]

-- 6. foldl' последовательно применяет функцию f к элементу списка l и значению, полученному на предыдущем шаге, начальное значение z
foldl' :: (a -> b -> a) -> a -> [b] -> a
foldl' _ z [] = z
foldl' f z (x:xs) = foldl' f (f z x) xs

-- 7. concat' принимает на вход два списка и возвращает их конкатенацию
concat' :: [a] -> [a] -> [a]
concat' [] y = y
concat' (x:xs) y = x : concat' xs y

-- concat3' принимает на вход три списка и возвращает их конкатенацию
concat3' :: [a] -> [a] -> [a] -> [a]
concat3' x y z = concat' (concat' x y) z

-- filterPar' возвращает список из элементов, для которых f cо вторым аргументом z возвращает True
filterPar' :: (a -> b -> Bool) -> b -> [a] -> [a]
filterPar' f z x = [y | y<-x, f y z]

-- 8. quickSort' возвращает его отсортированный список (0,5)
quickSort' :: Ord a => [a] -> [a]
quickSort' [] = []
quickSort' (x:xs) = concat3' (quickSort' a) b (quickSort' c)
    where
        a = filterPar' (<) x xs
        b = filterPar' (==) x (x:xs)
        c = filterPar' (>) x xs
