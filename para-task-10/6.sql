select City.Name, City.Population, Country.Population from Country
inner join City on Country.Code like City.CountryCode
group by City.Name
order by (City.Population * 1.0) / (Country.Population * 1.0) desc, City.Name desc
limit 20;
