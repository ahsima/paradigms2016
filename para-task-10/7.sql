select Country.Name from Country
inner join City on Country.Code like City.CountryCode 
group by Country.Name
having sum(City.Population) < Country.Population / 2
order by Country.Name asc;
