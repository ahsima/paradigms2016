select Country.Name, LiteracyRate.Rate from Country, LiteracyRate
where Country.Code like LiteracyRate.CountryCode
order by Rate desc, LiteracyRate.Year desc
limit 1;
