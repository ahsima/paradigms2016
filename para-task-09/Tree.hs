import Prelude hiding (lookup)

-- Реализовать двоичное дерево поиска без балансировки (4 балла)
data BinaryTree k v = Nil | Node k v (BinaryTree k v) (BinaryTree k v) deriving (Show, Eq)

-- “Ord k =>” требует, чтобы элементы типа k можно было сравнивать 
lookup :: Ord k => k -> BinaryTree k v -> Maybe v
lookup k Nil  = Nothing
lookup k (Node key v left right) | k == key      = Just v
                                 | k < key       = lookup k left
                                 | otherwise     = lookup k right

insert :: Ord k => k -> v -> BinaryTree k v -> BinaryTree k v
insert k v Nil = Node k v Nil Nil
insert k v (Node key val left right) | k < key   = Node key val (insert k v left) right
                                     | k == key  = Node key v left right
                                     | otherwise = Node key val left (insert k v right)

delete :: Ord k => k -> BinaryTree k v -> BinaryTree k v
delete k Nil = Nil
delete k (Node key val left Nil)    | k == key        = left
                                    | otherwise       = Node key val (delete k left) Nil
delete k (Node key val Nil right)   | k == key        = right
                                    | otherwise       = Node key val Nil (delete k right)
delete k (Node key val left right)  | k > key         = Node key val left (delete k right)
                                    | k < key         = Node key val (delete k left) right
                                    | otherwise       = Node skey (getval snode) left (delete skey right)
                                    where skey  = getkey snode
                                          snode = findmin right

findmin :: Ord k => BinaryTree k v -> BinaryTree k v
findmin (Node k v Nil right) = (Node k v Nil right)
findmin (Node k v left right) = findmin left

getkey :: Ord k => BinaryTree k v -> k
getkey (Node k v left right) = k

getval :: Ord k => BinaryTree k v -> v
getval (Node k v left right) = v

